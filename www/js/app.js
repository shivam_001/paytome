// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('app', ['ionic','ngCordova'])

.run(function($ionicPlatform) {
  // $ionicPlatform.ready(function() {
  //   // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
  //   // for form inputs)
  //   if(window.cordova && window.cordova.plugins.Keyboard) {
  //     cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
  //   }
  //   if(window.StatusBar) {
  //     StatusBar.styleDefault();
  //   }
  // });
})




.controller('AppCtrl', function($scope, $cordovaPush, $cordovaDialogs, $cordovaMedia, $cordovaToast, $ionicPlatform, $cordovaDevice,$http,$state) {
    $scope.notifications = [];
    alert("Eneterd");
   // var cordova_device = $cordovaDevice.getCordova();
    // call to register automatically upon device ready
    $ionicPlatform.ready(function () {
        alert(0)
        $scope.register();
    });


    // Register
    $scope.register = function () {
      alert(1)

//       Project Number - 480647201372
// API key        -  AIzaSyB8hvkVLyy1FQg97YYygu8Vug5ZKK72mWM
        // var config = null;

        // if (ionic.Platform.isAndroid()) {
          var   config = {
                "senderID": "480647201372" // REPLACE THIS WITH YOURS FROM GCM CONSOLE - also in the project URL like: https://console.developers.google.com/project/434205989073
            };
        // }
        // else if (ionic.Platform.isIOS()) {
        //     config = {
        //         "badge": "true",
        //         "sound": "true",
        //         "alert": "true"
        //     }
        // }
        // var config = {
        //         "badge": "true",
        //         "sound": "true",
        //         "alert": "true"
        //     }


        // for android
        $cordovaPush.register(config).then(function (result) {
            alert("Register success " + result);
            console.log("Register success " + result);
            $cordovaToast.showShortCenter('Registered for push notifications');
            $scope.registerDisabled=true;
            // ** NOTE: Android regid result comes back in the pushNotificationReceived, only iOS returned here
            // if (cordova_device == 'iOS') {

                $scope.regId = result;
                console.log("In iOS "+ $scope.regId)
                storeDeviceToken("ios");
            // }
        }, function (err) {
            console.log("Register error " + err)
        });
    }

    // Notification Received
   
    $scope.$on('pushNotificationReceived', function (event, notification) {
        // $cordovaPush:notificationReceived
       // alert('123456')
        console.log("77 Page "+JSON.stringify([notification]));
        
        // if (ionic.Platform.isAndroid()) {
        //     handleAndroid(notification);
        // }
        // else if (ionic.Platform.isIOS()) {
            handleIOS(notification);
            $scope.$apply(function () {
              alert(123);
                $scope.notifications.push(JSON.stringify(notification.alert));
            })
        // }
    });

    // Android Notification Received Handler
    function handleAndroid(notification) {
      alert("and")
        // ** NOTE: ** You could add code for when app is in foreground or not, or coming from coldstart here too
        //             via the console fields as shown.
        console.log("In foreground " + notification.foreground  + " Coldstart " + notification.coldstart);
        if (notification.event == "registered") {
            $scope.regId = notification.regid;
            storeDeviceToken("android");
        }
        else if (notification.event == "message") {
            $cordovaDialogs.alert(notification.message, "Push Notification Received");
            $scope.$apply(function () {
                $scope.notifications.push(JSON.stringify(notification.message));
            })
        }
        else if (notification.event == "error")
            $cordovaDialogs.alert(notification.msg, "Push notification error event");
        else $cordovaDialogs.alert(notification.event, "Push notification handler - Unprocessed Event");
    }

    // IOS Notification Received Handler get work on
    function handleIOS(notification) {

        console.log(JSON.stringify([notification])+"\n"+"---notification.foreground--"+notification.foreground)
        var Note = notification;
        console.log("Foreground:--"+Note.notification.foreground)
        // notification[0].notification.foreground
        // The app was already open but we'll still show the alert and sound the tone received this way. If you didn't check
        // for foreground here it would make a sound twice, once when received in background and upon opening it from clicking
        // the notification when this code runs (weird).
        // if (notification.foreground == "1") {
          if (Note.notification.foreground == "1") {
            // Play custom audio if a sound specified.
            // $state.go('search1');
            if (Note.notification.sound) {
                var mediaSrc = $cordovaMedia.newMedia(Note.notification.sound);
                mediaSrc.promise.then($cordovaMedia.play(mediaSrc.media));
            }

            if (Note.notification.body && Note.notification.messageFrom) {
                $cordovaDialogs.alert(Note.notification.body, Note.notification.messageFrom);
            }
            else $cordovaDialogs.alert(Note.notification.alert, "Push Notification Received");

            if (Note.notification.badge) {
              // alert("badge success or err")
                $cordovaPush.setBadgeNumber(Note.notification.badge).then(function (result) {
                  alert("Success--"+result)
                    console.log("Set badge success " + result)
                    // alert(result);
                }, function (err) {
                  alert("Error--"+err)
                    console.log("Set badge error " + err)
                });
            }
        }
        // Otherwise it was received in the background and reopened from the push notification. Badge is automatically cleared
        // in this case. You probably wouldn't be displaying anything at this point, this is here to show that you can process
        // the data in this situation.
        else {
          // alert("back g")
          // $state.go('search1');
            if (Note.notification.body && Note.notification.messageFrom) {
                $cordovaDialogs.alert(Note.notification.body, "(RECEIVED WHEN APP IN BACKGROUND) " + Note.notification.messageFrom);
            }
            else $cordovaDialogs.alert(Note.notification.alert, "(RECEIVED WHEN APP IN BACKGROUND) Push Notification Received");
        }
    }

    // Stores the device token in a db using node-pushserver (running locally in this case)
    //
    // type:  Platform type (ios, android etc)
    function storeDeviceToken(type) {
        // Create a random userid to store with it
        var user = { user: 'user' + Math.floor((Math.random() * 10000000) + 1), type: type, token: $scope.regId };
        console.log("Post token for registered device with data " + JSON.stringify(user));

        $http.post('http://192.168.1.16:8000/subscribe', JSON.stringify(user))
            .success(function (data, status) {
                console.log("Token stored, device is successfully subscribed to receive push notifications.");
            })
            .error(function (data, status) {
                console.log("Error storing device token." + data + " " + status)
            }
        );
    }

    // Removes the device token from the db via node-pushserver API unsubscribe (running locally in this case).
    // If you registered the same device with different userids, *ALL* will be removed. (It's recommended to register each
    // time the app opens which this currently does. However in many cases you will always receive the same device token as
    // previously so multiple userids will be created with the same token unless you add code to check).
    function removeDeviceToken() {
        var tkn = {"token": $scope.regId};
        $http.post('http://192.168.1.16:8000/unsubscribe', JSON.stringify(tkn))
            .success(function (data, status) {
                console.log("Token removed, device is successfully unsubscribed and will not receive push notifications.");
            })
            .error(function (data, status) {
                console.log("Error removing device token." + data + " " + status)
            }
        );
    }

    // Unregister - Unregister your device token from APNS or GCM
    // Not recommended:  See http://developer.android.com/google/gcm/adv.html#unreg-why
    //                   and https://developer.apple.com/library/ios/documentation/UIKit/Reference/UIApplication_Class/index.html#//apple_ref/occ/instm/UIApplication/unregisterForRemoteNotifications
    //
    // ** Instead, just remove the device token from your db and stop sending notifications **
    $scope.unregister = function () {
        console.log("Unregister called");
        removeDeviceToken();
        $scope.registerDisabled=false;
        //need to define options here, not sure what that needs to be but this is not recommended anyway
//        $cordovaPush.unregister(options).then(function(result) {
//            console.log("Unregister success " + result);//
//        }, function(err) {
//            console.log("Unregister error " + err)
//        });
    }


})
